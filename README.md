# Set Default Audio Volume

A bash script to set the volume of the default audio output with PusleAudio.
Meant to control volume with the keyboard in i3wm, without having to worry about the index/name of the sink.
You just have to set the default sink with this command:
```
pactl set-default-sink 1
```
Then add it to the i3 config file like this:
```
# Pulse Audio controls
bindsym XF86AudioRaiseVolume exec --no-startup-id ~/scripts/set-vol.sh +5% # increase sound volume
bindsym XF86AudioLowerVolume exec --no-startup-id ~/scripts/set-vol.sh -5% # decrease sound volume
bindsym XF86AudioMute exec --no-startup-id ~/scripts/set-vol.sh mute       # mute sound
```
The script adjusts the volume of the sink that is set as default.
Otherwise you'd have to change the sink name or index in i3 config all the time you change audio output, like if you want to use your wireless headset instead of your speakers.

An alternative to this approach is to adjust the volume of the RUNNING sick, in which case you don't have to worry about the default output. However, you can only adjust or mute volume if something is running. I personally like to set a default audio output to be able to adjust or mute the volume even if I'm not listening to anything at the moment.
